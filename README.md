Hola amigos he desarrollado el siguiente código que permite seleccionar  
solo a un checkbox de un grupo de 5 dentro de una tira de 3 preguntas,
a raíz de este desarrollo han surgidos dos incógnitas:

1.- ¿cómo se podría optimizar el código para la ubicación del checkbox  
y hacer que solo se pueda marcar un checkbox 
y desmarcar al resto dentro de  la fila?

Como podrán apreciar he tenido que agregar una función por cada grupo 
de checkbox la cual me permite obtener el checkbox cliqueado y una lista 
de los checkbox que pertenecen a esta  fila mediante su clase, sin 
embargo en este momento solo se trabaja con 3 preguntas pero, 
¿qué pasaría  si fueran 100 preguntas? 

Pues por cada pregunta se deben de agregar 4 líneas al código, 
volviéndolo cada vez más pesado y complejo.

2.- ¿existe otro método para saber si una pregunta de
la lista se ha quedado sin respuesta?

Como verán he usado una condición if con un valor  fijo de 3
ya que en este caso solo se tienen 3 preguntas, pero 
¿cómo podría mejorarse para cuando el cuestionario fuera dinámico que no 
se sepa que se tiene 10 o 15 o 20 preguntas?
